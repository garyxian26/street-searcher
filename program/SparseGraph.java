package hw9;

import exceptions.InsertionException;
import exceptions.PositionException;
import exceptions.RemovalException;

import java.util.ArrayList;
import java.util.List;

/**
    An implementation of a directed graph using incidence lists
    for sparse graphs where most things aren't connected.
    @param <V> Vertex element type.
    @param <E> Edge element type.
*/
public class SparseGraph<V, E> implements Graph<V, E> {

    // Class for a vertex of type V
    private final class VertexNode<V> implements Vertex<V> {
        V data;
        Graph<V, E> owner;
        List<Edge<E>> outgoing;
        List<Edge<E>> incoming;
        Object label;
        double dist;

        VertexNode(V v) {
            this.data = v;
            this.outgoing = new ArrayList<>();
            this.incoming = new ArrayList<>();
            this.label = null;
            this.dist = 0;
        }

        @Override
        public V get() {
            return this.data;
        }

        @Override
        public void put(V v) {
            this.data = v;
        }
    }

    //Class for an edge of type E
    private final class EdgeNode<E> implements Edge<E> {
        E data;
        Graph<V, E> owner;
        VertexNode<V> from;
        VertexNode<V> to;
        Object label;

        // Constructor for a new edge
        EdgeNode(VertexNode<V> f, VertexNode<V> t, E e) {
            this.from = f;
            this.to = t;
            this.data = e;
            this.label = null;
        }

        @Override
        public E get() {
            return this.data;
        }

        @Override
        public void put(E e) {
            this.data = e;
        }
    }

    private List<Vertex<V>> vertices;
    private List<Edge<E>> edges;

    /** Constructor for instantiating a graph. */
    public SparseGraph() {
        this.vertices = new ArrayList<>();
        this.edges = new ArrayList<>();
    }

    // Checks vertex belongs to this graph
    private void checkOwner(VertexNode<V> toTest) {
        if (toTest.owner != this) {
            throw new PositionException();
        }
    }

    // Checks edge belongs to this graph
    private void checkOwner(EdgeNode<E> toTest) {
        if (toTest.owner != this) {
            throw new PositionException();
        }
    }

    // Converts the vertex back to a VertexNode to use internally
    private VertexNode<V> convert(Vertex<V> v) throws PositionException {
        try {
            VertexNode<V> gv = (VertexNode<V>) v;
            this.checkOwner(gv);
            return gv;
        } catch (ClassCastException ex) {
            throw new PositionException();
        }
    }

    // Converts and edge back to a EdgeNode to use internally
    private EdgeNode<E> convert(Edge<E> e) throws PositionException {
        try {
            EdgeNode<E> ge = (EdgeNode<E>) e;
            this.checkOwner(ge);
            return ge;
        } catch (ClassCastException ex) {
            throw new PositionException();
        }
    }

    @Override
    public Vertex<V> insert(V v) {
        VertexNode<V> nv = new VertexNode<V>(v);
        nv.owner = this;
        this.vertices.add(nv);
        return nv;
    }

    @Override
    public Edge<E> insert(Vertex<V> from, Vertex<V> to, E e)
            throws PositionException, InsertionException {
        if (from == null || to == null) {
            throw new PositionException();
        }
        VertexNode<V> nv1 = convert(from);
        VertexNode<V> nv2 = convert(to);
        //no self loop
        if (nv1 == nv2) {
            throw new InsertionException();
        }
        //checks if edge already exists bewteen two nodes
        for (Edge<E> edg: nv1.outgoing) {
            if (convert(edg).to == nv2) {
                throw new InsertionException();
            }
        }
        EdgeNode<E> ne = new EdgeNode<E>(nv1, nv2, e);
        ne.owner = this;
        nv1.outgoing.add(ne);
        nv2.incoming.add(ne);
        this.edges.add(ne);
        return ne;
    }

    @Override
    public V remove(Vertex<V> v) throws PositionException,
            RemovalException {
        VertexNode<V> temp = convert(v);
        V data = temp.get();
        //checks if vertex has incident edges
        if (!temp.outgoing.isEmpty() || !temp.incoming.isEmpty()) {
            throw new RemovalException();
        }
        else {
            temp.owner = null;
            this.vertices.remove(temp);
            return data;
        }
    }

    @Override
    public E remove(Edge<E> e) throws PositionException {
        EdgeNode<E> temp = convert(e);
        E data = temp.get();
        temp.owner = null;
        temp.from.outgoing.remove(temp);
        temp.to.incoming.remove(temp);
        this.edges.remove(temp);
        return data;
    }

    @Override
    public Iterable<Vertex<V>> vertices() {
        List<Vertex<V>> newlist = new ArrayList<>();
        newlist.addAll(this.vertices);
        return newlist;  
    }

    @Override
    public Iterable<Edge<E>> edges() {
        List<Edge<E>> newlist = new ArrayList<>();
        newlist.addAll(this.edges);
        return newlist;  
    }

    @Override
    public Iterable<Edge<E>> outgoing(Vertex<V> v) throws PositionException {
        List<Edge<E>> newlist = new ArrayList<>();
        newlist.addAll(convert(v).outgoing);
        return newlist;
    }

    @Override
    public Iterable<Edge<E>> incoming(Vertex<V> v) throws PositionException {
        List<Edge<E>> newlist = new ArrayList<>();
        newlist.addAll(convert(v).incoming);
        return newlist;
    }

    @Override
    public Vertex<V> from(Edge<E> e) throws PositionException {
        return convert(e).from;
    }

    @Override
    public Vertex<V> to(Edge<E> e) throws PositionException {
        return convert(e).to;
    }

    @Override
    public void label(Vertex<V> v, Object l) throws PositionException {
        convert(v).label = l;
    }

    @Override
    public void label(Edge<E> e, Object l) throws PositionException {
        convert(e).label = l;
    }

    @Override
    public Object label(Vertex<V> v) throws PositionException {
        return convert(v).label;
    }

    @Override
    public Object label(Edge<E> e) throws PositionException {
        return convert(e).label;
    }

    @Override
    public void clearLabels() {
        for (Vertex<V> v : this.vertices) {
            convert(v).label = null;
        }
        for (Edge<E> e : this.edges) {
            convert(e).label = null;
        }
    }

    private String vertexString(Vertex<V> v) {
        return "\"" + v.get() + "\"";
    }

    private String verticesToString() {
        StringBuilder sb = new StringBuilder();
        for (Vertex<V> v : this.vertices) {
            sb.append("  ").append(vertexString(v)).append("\n");
        }
        return sb.toString();
    }

    private String edgeString(Edge<E> e) {
        return String.format("%s -> %s [label=\"%s\"]",
                this.vertexString(this.from(e)),
                this.vertexString(this.to(e)),
                e.get());
    }

    private String edgesToString() {
        String edgs = "";
        for (Edge<E> e : this.edges) {
            edgs += "    " + this.edgeString(e) + ";\n";
        }
        return edgs;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("digraph {\n")
                .append(this.verticesToString())
                .append(this.edgesToString())
                .append("}");
        return sb.toString();
    }

    /**
    * Function to get distance data member from Vertex.
    * @param v Vertex position to pass in 
    * @return the distance data 
    */
    public double getdist(Vertex<V> v) {
        return convert(v).dist;
    }

    /**
    * Function to set distance data member in Vertex.
    * @param v Vertex position to pass in  
    * @param dist distance to be set
    */
    public void setdist(Vertex<V> v, double dist) {
        convert(v).dist = dist;
    }
}
