package hw9.tests;

import hw9.Vertex;
import hw9.Edge;
import hw9.Graph;
import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;
import hw9.SparseGraph;
import java.lang.Boolean;

import exceptions.InsertionException;
import exceptions.PositionException;
import exceptions.RemovalException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public abstract class GraphTest {

    protected Graph<String, String> graph;

    protected abstract Graph<String, String> createGraph();

    protected Graph<String, String> diffgraph = new SparseGraph<>();

    @Before
    public void setupGraph() {
        this.graph = createGraph();
    }

    @Test
    public void insertVertexWorks() {
    	graph.insert("one");
    	assertEquals("digraph {\n  \"one\"\n}", graph.toString());
    } 

    @Test
    public void insertEdgeWorks() {
    	Vertex<String> first = graph.insert("one");
    	Vertex<String> second = graph.insert("two");
    	assertEquals("digraph {\n  \"one\"\n  \"two\"\n}", graph.toString());
    	graph.insert(first, second, "road");
    	assertEquals("digraph {\n  \"one\"\n  \"two\"\n    \"one\" -> \"two\" [label=\"road\"];\n}", graph.toString());
    }

    @Test (expected=PositionException.class) 
   	public void insertEdgeNullNode() {
   		Vertex<String> first = graph.insert("one");
   		graph.insert(first, null, "road");
   	}

    @Test (expected=PositionException.class) 
   	public void insertEdgeDiffGraphNode() {
   		Vertex<String> first = graph.insert("one");
   		Vertex<String> second = diffgraph.insert("two");
   		graph.insert(first, second, "road");
   	}

   	@Test (expected=InsertionException.class) 
   	public void insertDuplicateEdge() {
   		Vertex<String> first = graph.insert("one");
   		Vertex<String> second = graph.insert("two");
   		graph.insert(first, second, "road");
   		graph.insert(first, second, "road1");
   	}

   	@Test (expected=InsertionException.class) 
   	public void insertSelfLoopEdge() {
   		Vertex<String> first = graph.insert("one");
   		Vertex<String> second = graph.insert("two");
   		graph.insert(first, first, "road");
   	}

    @Test
    public void removeVertexWorks() {
    	Vertex<String> first = graph.insert("one");
    	Vertex<String> second = graph.insert("two");
    	assertEquals("digraph {\n  \"one\"\n  \"two\"\n}", graph.toString());
    	assertEquals("two", graph.remove(second));
    	assertEquals("digraph {\n  \"one\"\n}", graph.toString());
    }

   	@Test (expected=PositionException.class) 
   	public void removeVertexDiffGraph() {
   		Vertex<String> second = diffgraph.insert("two");
   		graph.remove(second);
   	}

   	@Test (expected=RemovalException.class) 
   	public void removeVertexWithIncidentEdge() {
   		Vertex<String> first = graph.insert("one");
   		Vertex<String> second = graph.insert("two");
   		graph.insert(first, second, "road");
   		graph.remove(first);
   	}

    @Test 
    public void removeEdgeWorks() {
    	Vertex<String> first = graph.insert("one");
    	Vertex<String> second = graph.insert("two");
    	Edge<String> firste = graph.insert(first, second, "road1");
    	assertEquals("digraph {\n  \"one\"\n  \"two\"\n    \"one\" -> \"two\" [label=\"road1\"];\n}", graph.toString());
    	assertEquals("road1", graph.remove(firste));
    	assertEquals("digraph {\n  \"one\"\n  \"two\"\n}", graph.toString());
    }

    @Test (expected=PositionException.class) 
   	public void removeEdgeDiffGraph() {
   		Vertex<String> first = diffgraph.insert("one");
   		Vertex<String> second = diffgraph.insert("two");
   		Edge<String> firste = diffgraph.insert(first, second, "road");
   		graph.remove(firste);
   	}

    @Test
    public void getVerticesWork() {
    	Vertex<String> first = graph.insert("one");
    	Vertex<String> second = graph.insert("two");
    	Iterable<Vertex<String>> iterable = graph.vertices();
    	Iterator<Vertex<String>> iter = iterable.iterator();
    	assertEquals("one", iter.next().get());
    	assertEquals("two", iter.next().get());
    }

 	@Test
    public void getEdgesWork() {
    	Vertex<String> first = graph.insert("one");
    	Vertex<String> second = graph.insert("two");
    	Vertex<String> third = graph.insert("two");
    	Iterable<Vertex<String>> iterable = graph.vertices();
    	Iterator<Vertex<String>> iter = iterable.iterator();
    	assertEquals("one", iter.next().get());
    	assertEquals("two", iter.next().get());
    }

    @Test
    public void testOutgoing() {
    	Vertex<String> first = graph.insert("one");
    	Vertex<String> second = graph.insert("two");
    	graph.insert(first, second, "road");
    	Iterable<Edge<String>> iterable = graph.outgoing(first);
    	Iterator<Edge<String>> iter = iterable.iterator();
    	assertEquals("road", iter.next().get());
    }

    @Test
    public void userCannotRemoveOutgoing() {
    	Vertex<String> first = graph.insert("one");
    	Vertex<String> second = graph.insert("two");
    	graph.insert(first, second, "road");
    	Iterable<Edge<String>> iterable = graph.outgoing(first);
    	Iterator<Edge<String>> iter = iterable.iterator();
    	iter.next();
    	iter.remove();
    	Iterable<Edge<String>> iterable1 = graph.outgoing(first);
    	Iterator<Edge<String>> iter1 = iterable1.iterator();
    	assertEquals("road", iter1.next().get());
    }

    @Test (expected=PositionException.class) 
   	public void outgoingDiffGraph() {
   		Vertex<String> first = diffgraph.insert("one");
   		graph.outgoing(first);
   	}

   	@Test
    public void testIncoming() {
    	Vertex<String> first = graph.insert("one");
    	Vertex<String> second = graph.insert("two");
    	graph.insert(first, second, "road");
    	Iterable<Edge<String>> iterable = graph.incoming(second);
    	Iterator<Edge<String>> iter = iterable.iterator();
    	assertEquals("road", iter.next().get());
    }

    @Test
    public void userCannotRemoveIncoming() {
    	Vertex<String> first = graph.insert("one");
    	Vertex<String> second = graph.insert("two");
    	graph.insert(first, second, "road");
    	Iterable<Edge<String>> iterable = graph.incoming(second);
    	Iterator<Edge<String>> iter = iterable.iterator();
    	iter.next();
    	iter.remove();
    	Iterable<Edge<String>> iterable1 = graph.incoming(second);
    	Iterator<Edge<String>> iter1 = iterable1.iterator();
    	assertEquals("road", iter1.next().get());
    }

    @Test (expected=PositionException.class) 
   	public void incomingDiffGraph() {
   		Vertex<String> first = diffgraph.insert("one");
   		graph.incoming(first);
   	}

   	@Test
    public void testFrom() {
    	Vertex<String> first = graph.insert("one");
    	Vertex<String> second = graph.insert("two");
    	Edge<String> firste = graph.insert(first, second, "road");
    	Vertex<String> temp = graph.from(firste);
    	assertEquals(first, temp);
    }

    @Test (expected=PositionException.class) 
   	public void fromDiffGraph() {
   		Vertex<String> first = diffgraph.insert("one");
   		Vertex<String> second = diffgraph.insert("two");
   		Edge<String> firste = diffgraph.insert(first, second, "road");
   		graph.from(firste);
   	}

   	@Test
    public void testTo() {
    	Vertex<String> first = graph.insert("one");
    	Vertex<String> second = graph.insert("two");
    	Edge<String> firste = graph.insert(first, second, "road");
    	Vertex<String> temp = graph.to(firste);
    	assertEquals(second, temp);
    }

    @Test (expected=PositionException.class) 
   	public void toDiffGraph() {
   		Vertex<String> first = diffgraph.insert("one");
   		Vertex<String> second = diffgraph.insert("two");
   		Edge<String> firste = diffgraph.insert(first, second, "road");
   		graph.to(firste);
   	}

   	@Test
   	public void testVertexLabel() {
   		Vertex<String> first = graph.insert("one");
   		Boolean visited = new Boolean(true);
   		graph.label(first, visited);
   		assertEquals(visited, graph.label(first));
   	}

   	@Test (expected=PositionException.class) 
   	public void testDiffGraphVertexLabel() {
   		Vertex<String> first = diffgraph.insert("one");
   		Boolean visited = new Boolean(true);
   		graph.label(first, visited);
   	}

   	@Test
   	public void testEdgeLabel() {
   		Vertex<String> first = graph.insert("one");
   		Vertex<String> second = graph.insert("two");
   		Edge<String> firste = graph.insert(first, second, "road");
   		Boolean visited = new Boolean(true);
   		graph.label(firste, visited);
   		assertEquals(visited, graph.label(firste));
   	}

    @Test (expected=PositionException.class) 
   	public void testDiffGraphEdgeLabel() {
   		Vertex<String> first = diffgraph.insert("one");
   		Vertex<String> second = diffgraph.insert("two");
   		Edge<String> firste = diffgraph.insert(first, second, "road");
   		Boolean visited = new Boolean(true);
   		graph.label(firste, visited);
   	}

   	@Test
   	public void testClearLabel() {
   		Vertex<String> first = graph.insert("one");
   		Vertex<String> second = graph.insert("two");
   		Edge<String> firste = graph.insert(first, second, "road");
   		Boolean visited = new Boolean(true);
   		String dist = new String("26km");
   		graph.label(first, visited);
   		graph.label(second, visited);
   		graph.label(firste, dist);
   		assertEquals(visited, graph.label(first));
   		assertEquals(visited, graph.label(second));
   		assertEquals(dist, graph.label(firste));
   		graph.clearLabels();
   		assertEquals(null, graph.label(first));
   		assertEquals(null, graph.label(second));
   		assertEquals(null, graph.label(firste));
   	}

   	@Test
   	public void testBooleanLabel() {
   		Vertex<String> first = graph.insert("one");
   		Vertex<String> second = graph.insert("two");
   		Boolean visited = new Boolean(true);
   		graph.label(first, visited);
   		graph.label(second, visited);
   		assertEquals("true", graph.label(first).toString());
   		assertEquals("true", graph.label(second).toString());
   	}

   	@Test
   	public void testStringLabel() {
   		Vertex<String> first = graph.insert("one");
   		Vertex<String> second = graph.insert("two");
   		Edge<String> firste = graph.insert(first, second, "road");
   		String dist = new String("26km");
   		graph.label(firste, dist);
   		assertEquals("26km", graph.label(firste).toString());
   	}

   	@Test
   	public void testUnDirectedEdge() {
   		Vertex<String> first = graph.insert("one");
   		Vertex<String> second = graph.insert("two");
   		Edge<String> firste = graph.insert(first, second, "road");
   		Edge<String> seconde = graph.insert(second, first, "road");
   		assertEquals("digraph {\n  \"one\"\n  \"two\"\n    \"one\" -> \"two\" [label=\"road\"];\n    \"two\" -> \"one\" [label=\"road\"];\n}", graph.toString());
   	}

   	@Test
   	public void makeIntersection() {
		Vertex<String> center = graph.insert("center");
   		Vertex<String> north = graph.insert("north");
   		Edge<String> nroad = graph.insert(center, north, "up");
   		Vertex<String> east = graph.insert("east");
   		Edge<String> eroad = graph.insert(center, east, "right");
   		Vertex<String> south = graph.insert("south");
   		Edge<String> sroad = graph.insert(center, south, "down");
   		Vertex<String> west = graph.insert("west");
   		Edge<String> wroad = graph.insert(center, west, "left");
   		assertEquals("digraph {\n  \"center\"\n  \"north\"\n  \"east\"\n  \"south\"\n  \"west\"\n    \"center\" -> \"north\" [label=\"up\"];\n    \"center\" -> \"east\" [label=\"right\"];\n    \"center\" -> \"south\" [label=\"down\"];\n    \"center\" -> \"west\" [label=\"left\"];\n}", graph.toString());
   	}

   	@Test
   	public void makeLongRoad() {
   		Vertex<String> first = graph.insert("first");
   		Vertex<String> second = graph.insert("second");
   		Vertex<String> third = graph.insert("third");
   		Vertex<String> fourth = graph.insert("fourth");
   		Edge<String> road1 = graph.insert(first, second, "road1");
   		Edge<String> road2 = graph.insert(second, third, "road2");
   		Edge<String> road3 = graph.insert(third, fourth, "road3");
   		assertEquals("digraph {\n  \"first\"\n  \"second\"\n  \"third\"\n  \"fourth\"\n    \"first\" -> \"second\" [label=\"road1\"];\n    \"second\" -> \"third\" [label=\"road2\"];\n    \"third\" -> \"fourth\" [label=\"road3\"];\n}", graph.toString());
   	}

   	@Test 
   	public void removeManyOutgoingEdges() {
   		Vertex<String> center = graph.insert("center");
   		Vertex<String> north = graph.insert("north");
   		Edge<String> nroad = graph.insert(center, north, "up");
   		Vertex<String> east = graph.insert("east");
   		Edge<String> eroad = graph.insert(center, east, "right");
   		Vertex<String> south = graph.insert("south");
   		Edge<String> sroad = graph.insert(center, south, "down");
   		Vertex<String> west = graph.insert("west");
   		Edge<String> wroad = graph.insert(center, west, "left");
   		Iterable<Edge<String>> iterable = graph.outgoing(center);
   		Iterator<Edge<String>> iter = iterable.iterator();
   		assertEquals("up", iter.next().get());
   		assertEquals("right", iter.next().get());
   		assertEquals("down", iter.next().get());
   		assertEquals("left", iter.next().get());
   		graph.remove(wroad);
   		graph.remove(sroad);
   		graph.remove(eroad);
   		Iterable<Edge<String>> iterable1 = graph.outgoing(center);
   		Iterator<Edge<String>> iter1 = iterable1.iterator();
   		assertEquals("up", iter1.next().get());
   		assertTrue(!iter.hasNext());
   	}

   	@Test 
   	public void removeManyIncomingEdges() {
   		Vertex<String> center = graph.insert("center");
   		Vertex<String> north = graph.insert("north");
   		Edge<String> nroad = graph.insert(north, center, "up");
   		Vertex<String> east = graph.insert("east");
   		Edge<String> eroad = graph.insert(east, center, "right");
   		Vertex<String> south = graph.insert("south");
   		Edge<String> sroad = graph.insert(south, center, "down");
   		Vertex<String> west = graph.insert("west");
   		Edge<String> wroad = graph.insert(west, center, "left");
   		Iterable<Edge<String>> iterable = graph.incoming(center);
   		Iterator<Edge<String>> iter = iterable.iterator();
   		assertEquals("up", iter.next().get());
   		assertEquals("right", iter.next().get());
   		assertEquals("down", iter.next().get());
   		assertEquals("left", iter.next().get());
   		graph.remove(wroad);
   		graph.remove(sroad);
   		graph.remove(eroad);
   		Iterable<Edge<String>> iterable1 = graph.incoming(center);
   		Iterator<Edge<String>> iter1 = iterable1.iterator();
   		assertEquals("up", iter1.next().get());
   		assertTrue(!iter.hasNext());
   	}

   	@Test
   	public void removeIsolatedVertex() {
   		Vertex<String> center = graph.insert("center");
   		Vertex<String> north = graph.insert("north");
   		Edge<String> nroad = graph.insert(center, north, "up");
   		Vertex<String> east = graph.insert("east");
   		Edge<String> eroad = graph.insert(center, east, "right");
   		graph.remove(eroad);
   		graph.remove(east);
   		graph.remove(nroad);
   		graph.remove(north);
   		graph.remove(center);
   	}

   	@Test
   	public void removeEdgeOutOfOrder() {
   		Vertex<String> center = graph.insert("center");
   		Vertex<String> north = graph.insert("north");
   		Edge<String> nroad = graph.insert(north, center, "up");
   		Vertex<String> east = graph.insert("east");
   		Edge<String> eroad = graph.insert(east, center, "right");
   		Vertex<String> south = graph.insert("south");
   		Edge<String> sroad = graph.insert(south, center, "down");
   		Vertex<String> west = graph.insert("west");
   		Edge<String> wroad = graph.insert(west, center, "left");
   		graph.remove(wroad);
   		graph.remove(sroad);
   		graph.remove(eroad);
   		Iterable<Edge<String>> iterable = graph.edges();
   		Iterator<Edge<String>> iter = iterable.iterator();
   		assertEquals("up", iter.next().get());
   		assertTrue(!iter.hasNext());
   	}

}
